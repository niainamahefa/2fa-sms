@extends('layouts.app')

@section('content')
    <article class="grid">
        <div>
            <hgroup>
                <h1>S'authentifier</h1>
                <h2>Authentification avec Google 2FA</h2>
            </hgroup>
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <input
                    id="email"
                    type="email"
                    class="form-control @error('email') is-invalid @enderror"
                    name="email" value="{{ old('email') }}"
                    required
                    autocomplete="email"
                    autofocus
                >
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <input
                    id="password"
                    type="password"
                    class="form-control @error('password') is-invalid @enderror"
                    name="password"
                    required
                    autocomplete="current-password"
                >

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <fieldset>
                    <label for="remember">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        {{ __('Se souvenir de moi') }}
                    </label>
                </fieldset>

                <button type="submit" class="contrast">
                    {{ __('Se connecter') }}
                </button>

                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Mot de passe oublié ?') }}
                    </a>
                @endif

            </form>
        </div>
        <div>
            <img src="{{ asset('/img/security.png') }}" width="90%"  alt="">
        </div>
    </article>
@endsection
