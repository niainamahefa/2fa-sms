@extends('layouts.app')

@section('content')
    <article class="grid">
        <div>
            <hgroup>
                <h1>🚀 S'inscrire</h1>
                <h2>Inscrivez-vous</h2>
            </hgroup>
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <label for="name" class="col-md-4 col-form-label text-md-end">👨 Nom </label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <label for="phone" class="col-md-4 col-form-label text-md-end">📱 Phone </label>
                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <label for="email" class="col-md-4 col-form-label text-md-end">📧 Adresse mail</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <label for="password" class="col-md-4 col-form-label text-md-end"> 🔐 Mot de passe</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <label for="password-confirm" class="col-md-4 col-form-label text-md-end">🔐 Confirmer le mot de passe</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                <fieldset>
                    <label for="remember">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        {{ __('Se souvenir de moi') }}
                    </label>
                </fieldset>

                <button type="submit" class="contrast">
                    S'inscrire
                </button>

            </form>
        </div>
    </article>
@endsection
