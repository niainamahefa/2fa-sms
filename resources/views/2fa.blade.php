@extends('layouts.app')

@section('content')
    <article class="grid">
        <div>
            <hgroup>
                <h1>Vérification par SMS</h1>
                <h3>Nous avons envoyé le code sur le numero : {{ substr(auth()->user()->phone, 0, 5) . '******' . substr(auth()->user()->phone,  -2) }}</h3>
            </hgroup>
            <form method="POST" action="{{ route('2fa.post') }}">
                @csrf

                @if ($message = Session::get('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($message = Session::get('error'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="form-group row">
                    <label for="code" class="col-md-4 col-form-label text-md-right">Code</label>

                    <div class="col-md-6">
                        <input id="code" type="number" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ old('code') }}" required autocomplete="code" autofocus>

                        @error('code')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <a class="btn btn-link" href="{{ route('2fa.resend') }}">Renvoyer le code?</a>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Se connecter
                        </button>

                    </div>
                </div>
            </form>
        </div>
        <div>
            <img src="{{ asset('/img/notification.png') }}"  width="65%" alt="">
        </div>
    </article>
@endsection
