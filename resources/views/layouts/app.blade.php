<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css">

    {{--@vite(['resources/sass/app.scss', 'resources/js/app.js'])--}}

</head>
<body>
    <!-- Nav -->
    <nav class="container-fluid">
        <ul>
            <li>
                <a href="./" class="contrast" onclick="event.preventDefault()"><h1>2FA</h1></a>
            </li>
        </ul>
        @if(auth()->user())
        <ul>
            <li>
                <details role="list" dir="rtl">
                    <summary aria-haspopup="listbox" role="link" class="secondary">{{ Auth::user()->name }}</summary>
                    <ul role="listbox">
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Déconnexion
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </details>
            </li>
        </ul>
        @endif
    </nav>
    <!-- ./ Nav -->
    <!-- Main -->
    <main class="container">
        @yield('content')
    </main>
    <!-- ./ Main -->
</body>
</html>
