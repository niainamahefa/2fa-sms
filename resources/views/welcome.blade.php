<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>2FA</title>
    <meta
        name="description"
        content="A basic custom template for Pico using only CSS custom properties (variables). Built with Pico CSS."
    />
    <link rel="shortcut icon" href="https://picocss.com/favicon.ico" />
    <link rel="canonical" href="https://picocss.com/examples/basic-template/" />

    <!-- Pico.css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css" />

    <!-- Custom template-->
    <link rel="stylesheet" href="css/custom.css" />
</head>

<body>
<!-- Main -->
<main class="container">
    <!-- Article-->
    <article id="article">
        <img src="{{asset('/img/laravel.png')}}" width="20%" alt=""> + <img src="{{asset('/img/twilio.png')}}" width="20%" alt="">
        <hgroup>
            <h1>Authentification à 2 facteurs</h1>
            <h3>Ceci est une simple application développée avec Laravel 10 qui l'authentification à 2 facteurs par SMS</h3>
        </hgroup>
        <a href="{{ route('login') }}" role="button" class="contrast">Se connecter</a>
        <a href="{{ route('register') }}" role="button">S'inscrire</a>
    </article>
    <!-- ./ Article-->
</main>
<!-- ./ Main -->
</body>
</html>
